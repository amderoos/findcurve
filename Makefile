# Generic Makefile for the compilation of a continuation problem of the class
# of models that can be analyzed with the generic program file FindCurve.c
#
# Possible targets to make:
#
#       make <model>			(builds executable from .h file)
#
#       make clean                      (cleans up all target programs)
#       make cleanoutput                (cleans up all error and output files)
#       make allclean                   (combines previous two)
#
#
#==============================================================================
# Specify where the library modules are located.
#==============================================================================

MODDIR = $(HOME)/programs/FindCurve

#==============================================================================
# Advanced tayloring
# The following settings might need adaptation for specific uses
#==============================================================================

# Determining the names of all potential target programs
# All .h files in the current directory are considered target programs

ALLHFILES = $(wildcard *.h)
ALLPROBLEMS = $(patsubst %.h,%,$(ALLHFILES))

# Alternatively, if not all .h files in the current directory are target programs
# specify the targets here:

# ALLPROBLEMS = <model1> <model2>

#==============================================================================
#  Compilation settings for GCC compiler
#==============================================================================

WARN = -Wall -Wpointer-arith -Wcast-qual -Wcast-align
INCLUDES = -I. -I$(MODDIR)

# Debugging settings
ifeq ("$(DEBUG)", $(filter "$(DEBUG)","debug" "1"))
CC	= clang
CFLAGS  = -ggdb -g3 -DDEBUG=1 $(INCLUDES) $(WARN)
TMPDIR  = .
else
CC	= clang
CFLAGS  = -O $(INCLUDES) $(WARN)
TMPDIR  = /tmp
endif

LDFLAGS  = -fno-common

#==============================================================================
# About the modules
#==============================================================================

MODULES = biftest curve dopri5 io
LIBS	= -llapack -lcblas -lm
DEBUGOBJS = $(patsubst %,./%.o,$(MODULES))
MODOBJS = $(patsubst %,$(TMPDIR)/%.o,$(MODULES))

#==============================================================================
# Defining the make targets to clean up
#==============================================================================

allclean: clean cleanoutput

clean: 
	@echo "Cleaning up all programs: "
	@for I in $(ALLPROBLEMS) ; do echo "Cleaning up $${I}...."; rm -f $${I} $${I}.o modules.lib.o $(DEBUGOBJS) ; done
	@rm -f *.mexmaci64 *.mexw64 *.mex *.o *.m~ *.so *.dll

cleanoutput:
	@echo "Cleaning up all error and output files: "
	@for I in $(ALLPROBLEMS) ; do rm -f $${I}*.bif $${I}*.csb $${I}*.err $${I}*.mat $${I}*.out ; done

#==============================================================================
# The dependencies of the executables, the problem-specific object file
# and the module library file
#==============================================================================

.SECONDARY:

%.o: %.h $(MODDIR)/FindCurve.c
	$(COMPILE.c) -DPROBLEMHEADER="$<" -o $@ $(MODDIR)/FindCurve.c

ifeq ("$(DEBUG)", $(filter "$(DEBUG)","debug" "1"))
modules.lib.o:
	for I in $(MODULES) ; do $(COMPILE.c) -o $(TMPDIR)/$${I}.o $(MODDIR)/$${I}.c ; done
	ld -r -o $@ $(MODOBJS)
else
modules.lib.o:
	for I in $(MODULES) ; do $(COMPILE.c) -o $(TMPDIR)/$${I}.o $(MODDIR)/$${I}.c ; done
	ld -r -o $@ $(MODOBJS)
	rm $(MODOBJS)
endif

%: %.o modules.lib.o
	$(LINK.c) $(LDFLAGS) -o $@ $^ $(LIBS)

#==============================================================================
