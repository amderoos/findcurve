function [curvepoints, bifpoints, biftypes, curvedesc] = FindCurve(modelname, biftype, startpoint, stepsize, bounds, parameters, options, varargin)

%
% FindCurve: Computes a bifurcation curve determined by a system of
%            non-linear equations
%
% Syntax:
%
%   [curvepoints, bifpoints, biftypes, curvedesc] = ...
%       FindCurve(modelname, biftype, startpoint, stepsize, bounds,...
%                 parameters, options, 'clear', 'force', 'debug')
%
% Arguments:
%
%   modelname:  (string, required)
%               Basename of the file with model specification. The file
%               should have extension '.h'. For example, the model 'PNAS2002'
%               is specified in the file 'PNAS2002.h'
%
%   biftype:    (string, required)
%               Type of bifurcation to compute: BP, EQ, LP or EXT
%
%   startpoint: (row vector, required)
%               The initial point from which to start the continuation of
%               the curve
%
%   stepsize:   (double, required)
%               The target step size along the computed equilibrium curve
%
%   bounds:     (row vector of length 2, 4 or twice the number of unknowns, required)
%               The bounds to the region to which to restrict the computation
%               of the curve. To only put restrictions on the parameter
%               values, the vector should be of length 2 or 4 in case
%               of a one or two parameter continuation, respectively.
%               Otherwise, the vector should be of a length twice the
%               number of unknowns in the problem. 
%               The format is always [min1 max1 min2 max2....]
%
%   parameters: (row vector, required, but can be the empty vector [])
%               Vector of length PARAMETER_NR (set in the model program
%               file), specifying the values for the model parameters to
%               use in the computation. Vectors of other lengths, including
%               an empty vector will be ignored.
%
%   options:    (cell array, required, but can be the empty cell array {})
%               Cell array with pairs of an option name and a value (for
%               example {'popBP', '1'}) or single options (i.e. 'test').
%               Possible option names and their values are:
%
%               'par1',   '<index>': Index of first bifurcation parameter
%               'par2',   '<index>': Index of second bifurcation parameter
%               'EXTfun', '<index>': Index of the element in the ExtraOutput[]
%                                    vector, for which to test for maximum
%                                    and minimum values along the curve
%               'EXTpar', '<index>': Index of the parameter, with respect
%                                    to which to test for maximum and
%                                    minimum values along the curve
%               'test'             : Perform only a single computation, 
%                                    reporting the values of the unknowns and
%                                    the value of the resulting systems of 
%                                    non-liear equations
%
%   'clean':    (string. optional argument)
%               Clear all the result files of the model before the
%               computation
%
%   'force':    (string, optional argument)
%               Force a rebuilding of the model before the computation
%
%   'debug':    (string, optional argument)
%               Compile the model in verbose mode and with debugging flag set
%
% Output:
%
%   curvepoints: Matrix with output for all computed points along the curve
%
%   bifpoints:   Matrix with the located bifurcation points along the curve
%
%   biftypes:    Column vector of strings, containing a description of the
%                type of bifurcation for each of the located bifurcation points
%
%   curvedesc:   Column vector with strings, summarizing the numerical details
%                of the computed curve (i.e., initial point, parameter values,
%                numerical settings used).
%

forcebuild = 0;
debugging = 0;
nVarargs = length(varargin);
curvepoints = [];
bifpoints = [];
biftypes = [];
curvedesc = [];
fname = '';
hfile = [modelname,'.h'];
fullfilename = mfilename('fullpath');
coredir = fileparts(fullfilename);
is_octave = exist ('OCTAVE_VERSION', 'builtin');

for k = 1:nVarargs
    if ischar(varargin{k})
        if strcmp(varargin{k}, 'clean')
            resfnames = strcat(modelname, '-', '*', '-', '*');
            delete(strcat(resfnames, '.bif'));
            delete(strcat(resfnames, '.err'));
            delete(strcat(resfnames, '.out'));
        elseif strcmp(varargin{k}, 'force') || strcmp(varargin{k}, 'forcebuild')
            forcebuild = 1;
        elseif strcmp(varargin{k}, 'debug')
            debugging = 1;
        else
            msg = ['Unknown function argument: ', varargin{k}];
            disp(' ');
            disp(msg);
            disp(' ');
            
        end
    end
end

if ~exist(hfile, 'file')
    msg = ['Model source file ', hfile, ' does not exist'];
    disp(' ');
    disp(msg);
    disp(' ');
    return;
end

ext = mexext;
exename = [modelname, '.', ext];

if (mislocked(modelname))
    munlock(modelname);
end
clear(modelname);

if ~exist(exename, 'file')
    build = true;
else
    d1 = dir(hfile);
    d2 = dir(exename);
    build = d1.datenum > d2.datenum;
end

if build || forcebuild
    if exist(exename, 'file')
        delete(exename);
    end
    msg = ['Building executable', ' ', exename, ' ', '...'];
    disp(' ');
    disp(msg);
    disp(' ');
    if (is_octave)
        % To build the file using Octave, only tested on Mac OS:
        if debugging
            optstring = ' -g -v';
        else
            optstring = '';
        end
        outstring   = [' -o ' exename ];
        cflagstring = ' -Wall -DOCTAVE_MEX_FILE';
        lapacklib   = ' -llapack';
        blaslib     = ' -lblas';
        utlib       = '';
    else
        % To build the file using Matlab
        if debugging
            optstring = ' -g -v  -largeArrayDims';
        else
            optstring = ' -O  -largeArrayDims';
        end
        outstring   = [' -output ' exename ];
        if strcmp(ext, 'mexw64')
            % To build the file on Windows using VC:
            cflagstring = ' COMPFLAGS="$COMPFLAGS -Wall"';
            lapacklib = [' "' fullfile(matlabroot, ...
                'extern', 'lib', computer('arch'), 'microsoft', 'libmwlapack.lib') '"'];
            blaslib = [' "' fullfile(matlabroot, ...
                'extern', 'lib', computer('arch'), 'microsoft', 'libmwblas.lib') '"'];
            utlib = [' "' fullfile(matlabroot, ...
                'extern', 'lib', computer('arch'), 'microsoft', 'libut.lib') '"'];
        else
            % To build the file on UNIX:
            cflagstring = ' CFLAGS=''$CFLAGS -Wall''';
            lapacklib   = ' -lmwlapack';
            blaslib     = ' -lmwblas';
            utlib       = ' -lut';
        end
    end
    
    cmd = ['mex' optstring outstring cflagstring];
    cmd = [cmd ' -DPROBLEMHEADER=' hfile ' -I. -I"' coredir '"'];
    
    cmd = [cmd ' "' coredir '/FindCurve.c" "' coredir '/biftest.c"'];
    cmd = [cmd ' "' coredir '/curve.c" "' coredir '/dopri5.c"'];
    cmd = [cmd ' "' coredir '/io.c"'];
    cmd = [cmd lapacklib blaslib utlib ];
    
    if debugging
        disp('');
        disp('Command line:');
        disp('');
        disp(cmd);
        disp('');
    end
    
    eval(cmd);
    if ~exist(exename, 'file')
        msg = ['Compilation of file ', exename, ' failed'];
        disp(' ');
        disp(msg);
        disp(' ');
        return;
    end
    if is_octave && ~debugging
        delete('FindCurve.o');
        delete('biftest.o');
        delete('curve.o');
        delete('dopri5.o');
        delete('io.o');
    end
else
    msg = ['Executable ', exename, ' is up-to-date'];
    disp(' ');
    disp(msg);
    disp(' ');
    feval('munlock', modelname);
    feval('clear', modelname);
end

drawnow;

fname = feval(modelname, biftype, startpoint, stepsize, bounds, parameters, options);

if (~isempty(fname))
    if exist(strcat(fname, '.out'), 'file')
        fid = fopen(strcat(fname, '.out'));
        line = fgetl(fid);
        headerlines = 0;
        while ischar(line)
            if line(1) == '#'
                headerlines = headerlines + 1;
%                 curvedesc{headerlines} = line;
                curvedesc = [curvedesc '\n' line];
            else
                fclose(fid);
                curvepoints = dlmread(strcat(fname, '.out'), '', headerlines, 0);
                break;
            end
            line = fgetl(fid);
        end
        if isempty(curvepoints)
            fclose(fid);
        end
        curvedesc = [curvedesc '\n'];
        fprintf(curvedesc);
    end
    
    if exist(strcat(fname, '.bif'), 'file')
        fid = fopen(strcat(fname, '.bif'));
        line = fgetl(fid);
        while ischar(line)
            pos = strfind(line, '****');
            if isempty(pos) 
                line = fgetl(fid); 
                continue; 
            end;
            part1 = line(1:(pos(1)-1));
            part2 = strtrim(line((pos(1)+4):(pos(2)-1)));
            while (length(part2) < 3)
                part2 = [part2 ' '];
            end
            row = textscan(part1, '%f');
            bifpoints = [bifpoints; transpose(cell2mat(row))];
            biftypes = [biftypes; part2];
            line = fgetl(fid);
        end
        fclose(fid);
    end
end
end