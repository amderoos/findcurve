disp(' ');
disp('=============================================================================================================================');
disp(' ');
disp('Clean all executables and test results');
disp(' ');
prompt = 'Do you want to proceed (default), skip (hit S), or quit (hit Q)? Y/S/Q [Y]: ';
str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end
if ~strcmp(str, 'q') && ~strcmp(str, 'Q') && ~strcmp(str, 's') && ~strcmp(str, 'S')
    clear
    close all
    allsrcs = dir('*.h');
    for i = 1:length(allsrcs)
        [cdir, modelname] = fileparts(allsrcs(i).name);
        exename = [modelname, '.', mexext];
        if exist(exename, 'file')
            delete(exename);
        end
        exename = [modelname, '.mex'];
        if exist(exename, 'file')
            delete(exename);
        end
        resfnames = strcat(modelname, '-', '*', '-', '*');
        delete(strcat(resfnames, '.bif'));
        delete(strcat(resfnames, '.err'));
        delete(strcat(resfnames, '.out'));
    end
end