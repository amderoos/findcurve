clear 
close all
scrsz = get(0,'ScreenSize');
left= 0.1;
width=0.85;
    
disp(' ');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

parameters = [0.1, 30.0, 1.0, 0.1, 0.5, 0.1, 0.015, 0.5, 3.0, 0.0, 0.0, 0.0, 0.0, 10.0, 0.0, 0.32, 0.032, 0.5, 0.005, 0.0, 3.0, 0.0, 1.0, 0.0];

disp(' '); disp(' ');
disp(' ');
disp('Testing juvenile biomass overcompensation detection in the consumer-resource equilibrium');
disp(' ');
prompt = '>> [data1, bdata1, btype1, desc1] = FindCurve(''CjaP'', ''EQ'', [0.0, 2.12019, 0.892170, 11.3152, 0.0], 0.5, [0.0 1.0], parameters, {''par1'', ''9'', ''EXTfun'', ''1'', ''EXTpar'', ''9''}, ''clean'', ''force'')';
str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end
if strcmp(str, 'q') || strcmp(str, 'Q')
    error('Test script interrupted by user');
elseif ~strcmp(str, 's') && ~strcmp(str, 'S')
    [data1, bdata1, btype1, desc1] = FindCurve('CjaP', 'EQ', [0.0 2.12019 0.892170 11.3152 0.0], 0.5, [0.0 1.0], parameters, {'par1', '9', 'EXTfun', '1', 'EXTpar', '9'}, 'clean', 'force');
    
    hfig = figure('Position',[1 scrsz(4) scrsz(3)/2 scrsz(4)/2]);
    h1 = axes('Position',[left 0.12 width width], 'LineWidth', 1, 'XLim', [0.0,0.5], 'YLim', [0.0, 3.0]);
    hx1=xlabel('Additional consumer mortality');
    hy1=ylabel('Average juvenile density');
    set(gca, 'fontsize', 16);
    set(hx1, 'fontsize', 18);
    set(hy1, 'fontsize', 18);
    
    % Plot the data
    axes(h1);
    hold on
    plot(data1(:,1), data1(:,8),'Color',[.6 0 0]);
    plot(bdata1(:,1), bdata1(:,8),'*', 'Color','red');
    text(bdata1(:,1), bdata1(:,8),btype1);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(' '); disp(' ');
disp(' ');
disp('Continuation of the juvenile biomass overcompensation boundary in the consumer-resource equilibrium as a function of consumer mortality and interval');
disp(' ');
prompt = '>> [data2, bdata2, btype2, desc2] = FindCurve(''CjaP'', ''EXT'', bdata1(1,1:6), 0.5, [0.0 1.0 0 70], parameters, {''par1'', ''9'', ''par2'', ''13'', ''EXTfun'', ''1'', ''EXTpar'', ''9''})';
str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end
if strcmp(str, 'q') || strcmp(str, 'Q')
    error('Test script interrupted by user');
elseif ~strcmp(str, 's') && ~strcmp(str, 'S')
    [data2, bdata2, btype2, desc2] = FindCurve('CjaP', 'EXT', bdata1(1,1:6), 0.5, [0.0 1.0 0 70], parameters, {'par1', '9', 'par2', '13', 'EXTfun', '1', 'EXTpar', '9'});
    
    [data3, bdata3, btype3, desc3] = FindCurve('CjaP', 'EXT', bdata1(1,1:6), -0.5, [0.0 1.0 0 70], parameters, {'par1', '9', 'par2', '13', 'EXTfun', '1', 'EXTpar', '9'});

    if exist('hfig','var')
        figure(hfig);
    end
    hfig2 = figure('Position',[1 1 scrsz(3)/2 scrsz(4)/2]);
    h2 = axes('Position',[left 0.12 width width], 'LineWidth', 1, 'XLim', [0.0,0.5], 'YLim', [0.0, 70.0]);
    hx2=xlabel('Additional consumer mortality');
    hy2=ylabel('Interval');
    set(gca, 'fontsize', 16);
    set(hx2, 'fontsize', 18);
    set(hy2, 'fontsize', 18);
    
    % Plot the data
    axes(h2);
    hold on
    plot(data2(:,1), data2(:,6),'Color',[.6 0 0]);
    plot(data3(:,1), data3(:,6),'Color',[.6 0 0]);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(' '); disp(' ');
disp(' ');
disp('Continuation of the predator extinction boundary as a function of consumer mortality and interval');
disp(' ');
prompt = '>> [data2, bdata2, btype2, desc2] = FindCurve(''CjaP'', ''BP'', bdata1(2,1:6), 0.5, [0.0 1.0 0 70], parameters, {''par1'', ''9'', ''par2'', ''13''})';
str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end
if strcmp(str, 'q') || strcmp(str, 'Q')
    error('Test script interrupted by user');
elseif ~strcmp(str, 's') && ~strcmp(str, 'S')
    [data2, bdata2, btype2, desc2] = FindCurve('CjaP', 'BP', bdata1(2,1:6), 0.5, [0.0 1.0 0 70], parameters, {'par1', '9', 'par2', '13'});
    
    [data3, bdata3, btype3, desc3] = FindCurve('CjaP', 'BP', bdata1(2,1:6), -0.5, [0.0 1.0 0 70], parameters, {'par1', '9', 'par2', '13'});

    % Plot the data
    if exist('hfig','var')
        figure(hfig);
    end
    if exist('hfig2','var')
        figure(hfig2);
    end
    axes(h2);
    hold on
    plot(data2(:,1), data2(:,6),'Color',[.6 0 0]);
    plot(data3(:,1), data3(:,6),'Color',[.6 0 0]);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

parameters = [0.1, 30.0, 1.0, 0.1, 0.5, 0.1, 0.015, 1.5, 3.0, 0.0, 0.0, 0.0, 0.0, 70.0, 0.0, 0.32, 0.032, 0.5, 0.005, 0.0, 3.0, 0.0, 1.0, 0.0];

disp(' '); disp(' ');
disp(' ');
disp('Testing saddle-node bifurcation detection in the predator consumer-resource equilibrium');
disp(' ');
prompt = '>> [data1, bdata1, btype1, desc1] = FindCurve(''CjaP'', ''EQ'', [0.0 27.0396 0.590107 0.132521 1.84090], 0.2, [0.0 1.0], [], {''par1'', ''21'', ''par2'', ''13''})';
str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end
if strcmp(str, 'q') || strcmp(str, 'Q')
    error('Test script interrupted by user');
elseif ~strcmp(str, 's') && ~strcmp(str, 'S')
    [data1, bdata1, btype1, desc1] = FindCurve('CjaP', 'EQ', [0.0, 27.0396, 0.590107, 0.132521, 1.84090], 0.2, [0.0 1.0], parameters, {'par1', '21', 'par2', '13'});
    
    hfig3 = figure('Position',[1 scrsz(4) scrsz(3)/2 scrsz(4)/2]);
    h3 = axes('Position',[left 0.12 width width], 'LineWidth', 1, 'XLim', [0.0,0.1], 'YLim', [0.0, 5.0]);
    hx3=xlabel('Additional predator mortality');
    hy3=ylabel('Average predator density');
    set(gca, 'fontsize', 16);
    set(hx3, 'fontsize', 18);
    set(hy3, 'fontsize', 18);
    
    % Plot the data
    if exist('hfig','var')
        figure(hfig);
    end
    if exist('hfig2','var')
        figure(hfig2);
    end
    if exist('hfig3','var')
        figure(hfig3);
    end
    axes(h3);
    hold on
    plot(data1(:,1), data1(:,11),'Color',[.6 0 0]);
    plot(bdata1(:,1), bdata1(:,11),'*', 'Color','red');
    text(bdata1(:,1), bdata1(:,11),btype1);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(' '); disp(' ');
disp(' ');
disp('Continuation of the saddle-node bifurcation as a function of predator mortality and interval');
disp(' ');
prompt = '>> [data2, bdata2, btype2, desc2] = FindCurve(''CjaP'', ''LP'', bdata1(1,1:6), -0.2, [0.0 1.0 0 70], , parameters, {''par1'', ''21'', ''par2'', ''13''})';
str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end
if strcmp(str, 'q') || strcmp(str, 'Q')
    error('Test script interrupted by user');
elseif ~strcmp(str, 's') && ~strcmp(str, 'S')
    [data2, bdata2, btype2, desc2] = FindCurve('CjaP', 'LP', bdata1(1,1:6), -0.2, [0.0 1.0 0 70], parameters, {'par1', '21', 'par2', '13'});
    
    [data3, bdata3, btype3, desc3] = FindCurve('CjaP', 'LP', bdata1(2,1:6), -0.2, [0.0 1.0 0 70], parameters, {'par1', '21', 'par2', '13'});

    if exist('hfig','var')
        figure(hfig);
    end
    if exist('hfig2','var')
        figure(hfig2);
    end
    if exist('hfig3','var')
        figure(hfig3);
    end
    hfig4 = figure('Position',[1 1 scrsz(3)/2 scrsz(4)/2]);
    h4 = axes('Position',[left 0.12 width width], 'LineWidth', 1, 'XLim', [0.0,0.1], 'YLim', [0.0, 70.0]);
    hx4=xlabel('Additional predator mortality');
    hy4=ylabel('Interval');
    set(gca, 'fontsize', 16);
    set(hx4, 'fontsize', 18);
    set(hy4, 'fontsize', 18);
    
    % Plot the data
    axes(h4);
    hold on
    plot(data2(:,1), data2(:,6),'Color',[.6 0 0]);
    plot(data3(:,1), data3(:,6),'Color',[.6 0 0]);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(' '); disp(' ');
disp(' ');
disp('Continuation of the predator invasion boundary as a function of predator mortality and interval');
disp(' ');
prompt = '>> [data4, bdata4, btype4, desc4] = FindCurve(''CjaP'', ''BP'', bdata1(3,1:6), -0.2, [0.0 1.0 0 70], parameters, {''par1'', ''21'', ''par2'', ''13''})';
str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end
if strcmp(str, 'q') || strcmp(str, 'Q')
    error('Test script interrupted by user');
elseif ~strcmp(str, 's') && ~strcmp(str, 'S')
    [data4, bdata4, btype4, desc4] = FindCurve('CjaP', 'BP', bdata1(3,1:6), -0.2, [0.0 1.0 0 70], parameters, {'par1', '21', 'par2', '13'});
    
    if exist('hfig','var')
        figure(hfig);
    end
    if exist('hfig2','var')
        figure(hfig2);
    end
    if exist('hfig3','var')
        figure(hfig3);
    end
    
    % Plot the data
    axes(h4);
    hold on
    plot(data4(:,1), data4(:,6),'Color',[.6 0 0]);
end
