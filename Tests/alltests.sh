# Change the parameter set to the default
# (echo 88c && echo "    { 0.1, 30.0, 1.0, 0.1, 0.5, 0.1, 0.015, 0.5, 3.0, 0.0, 0.0, 0.0, 0.0, 10.0, 0.0," && echo '.' && echo w) | ed - CjaP.h
(echo 90c && echo "double parameter[PARAMETER_NR] = {0.1, 30.0, 1.0, 0.1,  0.5,   0.1, 0.015, 0.5, 3.0, 0.0, 0.0, 0.0,"  && echo '.' && echo w) | ed - CjaP.h
(echo 91c && echo "                                  0.0, 10.0, 0.0, 0.32, 0.032, 0.5, 0.005, 0.0, 3.0, 0.0, 1.0, 0.0};" && echo '.' && echo w) | ed - CjaP.h

make -f ../Makefile allclean CjaP

echo ' ' ; echo ' '
echo '==========================================================================================================================================================
'
echo ' '
echo Testing the detection of a maximum in the juvenile biomass curve and the transcritical bifurcation in the system without predators
echo ' '
echo Executing: ./CjaP -par1 9 -EXTfun 1 -EXTpar 9 EQ 0.0 2.12019 0.892170 11.3152 0.0 0.1 0 1.0
echo ' '
read -p "Press [Enter] key to start test..."

time ./CjaP -par1 9 -EXTfun 1 -EXTpar 9 EQ 0.0 2.12019 0.892170 11.3152 0.0 0.1 0 1.0

# Change the parameter set
# (echo 88c && echo "    { 0.1, 30.0, 1.0, 0.1, 0.5, 0.1, 0.015, 1.5, 3.0, 0.0, 0.0, 0.0, 0.0, 70.0, 0.0," && echo '.' && echo w) | ed - CjaP.h
(echo 90c && echo "double parameter[PARAMETER_NR] = {0.1, 30.0, 1.0, 0.1,  0.5,   0.1, 0.015, 1.5, 3.0, 0.0, 0.0, 0.0,"  && echo '.' && echo w) | ed - CjaP.h
(echo 91c && echo "                                  0.0, 70.0, 0.0, 0.32, 0.032, 0.5, 0.005, 0.0, 3.0, 0.0, 1.0, 0.0};" && echo '.' && echo w) | ed - CjaP.h
make -f ../Makefile CjaP

echo ' ' ; echo ' '
echo '=========================================================================================================================================================='
echo ' '
echo Testing the detection of a maximum in the adult biomass curve and the transcritical bifurcation in the system without predators
echo ' '
echo Executing: ./CjaP -par1 9 -EXTfun 2 -EXTpar 9 EQ 0.0 2.51233117 20.28427404 1.69680616 0.0 0.1 0 1
echo ' '
read -p "Press [Enter] key to start test..."

time ./CjaP -par1 9 -EXTfun 2 -EXTpar 9 EQ 0.0 2.51233117 20.28427404 1.69680616 0.0 0.1 0 1

echo ' ' ; echo ' '
echo '=========================================================================================================================================================='
echo ' '
echo Continuation of the maximum in the adult biomass curve in the system without predators as a function of background mortality and growth season duration
echo ' '
echo Executing: ./CjaP -par1 9 -par2 13 -EXTfun 2 -EXTpar 9 EXT 0.0105301969 3.09464343 18.0910932 1.89398932  0.0 70 0.1 0 1 0 70
echo ' '
read -p "Press [Enter] key to start test..."

time ./CjaP -par1 9 -par2 13 -EXTfun 2 -EXTpar 9 EXT 0.0105301969 3.09464343 18.0910932 1.89398932  0.0 70 0.1 0 1 0 70

echo ' ' ; echo ' '
echo '=========================================================================================================================================================='
echo ' '
echo Continuation of the predator invasion threshold as a function of consumer background mortality and growth season duration
echo ' '
echo Executing: ./CjaP -par1 9 -par2 13 BP 7.53641E-02 2.88394E+01 4.86574E-01 4.51493E-02 0.0 70.0 0.1 0 1 0 70
echo ' '
read -p "Press [Enter] key to start test..."

time ./CjaP -par1 9 -par2 13 BP 7.53641E-02 2.88394E+01 4.86574E-01 4.51493E-02 0.0 70.0 0.1 0 1 0 70

echo ' ' ; echo ' '
echo '==========================================================================================================================================================
'
echo ' '
echo Continuation of the consumer invasion threshold as a function of consumer background mortality and growth season duration
echo ' '
echo Executing: ./CjaP -par1 9 -par2 13 BP 7.59283E-02 3.00000E+01 0.0 0.0 0.0 70.0 0.02 0 1 0 70
echo ' '
read -p "Press [Enter] key to start test..."

time ./CjaP -par1 9 -par2 13 BP 7.59283E-02 3.00000E+01 0.0 0.0 0.0 70.0 0.02 0 1 0 70

# Change the parameter set
# (echo 88c && echo "    { 0.1, 30.0, 1.0, 0.1, 0.5, 0.1, 0.015, 1.5, 3.0, 0.0, 0.0, 0.0, 0.0, 10.0, 0.0," && echo '.' && echo w) | ed - CjaP.h
(echo 90c && echo "double parameter[PARAMETER_NR] = {0.1, 30.0, 1.0, 0.1,  0.5,   0.1, 0.015, 1.5, 3.0, 0.0, 0.0, 0.0,"  && echo '.' && echo w) | ed - CjaP.h
(echo 91c && echo "                                  0.0, 10.0, 0.0, 0.32, 0.032, 0.5, 0.005, 0.0, 3.0, 0.0, 1.0, 0.0};" && echo '.' && echo w) | ed - CjaP.h
make -f ../Makefile allclean CjaP

echo ' ' ; echo ' '
echo '=========================================================================================================================================================='
echo ' '
echo Testing saddle-node bifurcation detection in the full system
echo ' '
echo Executing: ./CjaP -par1 21 -par2 13 EQ 0.0 10.97846641 3.52296961 0.51703890 6.98697114 0.1 0 1
echo ' '
read -p "Press [Enter] key to start test..."

time ./CjaP -par1 21 -par2 13 EQ 0.0 10.97846641 3.52296961 0.51703890 6.98697114 0.1 0 1

echo ' ' ; echo ' '
echo '=========================================================================================================================================================='
echo ' '
echo 'Continuation of the saddle-node bifurcation curve in 2 parameters (maximum resource density and background mortality)'
echo ' '
echo Excuting: ./CjaP -par1 21 -par2 13 LP 5.85888E-03 5.07232E+00 6.31391E+00 6.75657E-01 5.60637E+00 10.0 0.02 0 1 0 20
echo ' '
read -p "Press [Enter] key to start test..."

time ./CjaP -par1 21 -par2 13 LP 5.85888E-03 5.07232E+00 6.31391E+00 6.75657E-01 5.60637E+00 10.0 0.02 0 1 0 20

# Change the parameter set back to the default
# (echo 88c && echo "    { 0.1, 30.0, 1.0, 0.1, 0.5, 0.1, 0.015, 0.5, 3.0, 0.0, 0.0, 0.0, 0.0, 10.0, 0.0," && echo '.' && echo w) | ed - CjaP.h
(echo 90c && echo "double parameter[PARAMETER_NR] = {0.1, 30.0, 1.0, 0.1,  0.5,   0.1, 0.015, 0.5, 3.0, 0.0, 0.0, 0.0,"  && echo '.' && echo w) | ed - CjaP.h
(echo 91c && echo "                                  0.0, 10.0, 0.0, 0.32, 0.032, 0.5, 0.005, 0.0, 3.0, 0.0, 1.0, 0.0};" && echo '.' && echo w) | ed - CjaP.h

echo ' ' ; echo ' '
echo '=========================================================================================================================================================='
echo ' '
read -p "Press [Enter] key to clean all test results, <CTRL-C> to prevent it..."
make -f ../Makefile allclean

