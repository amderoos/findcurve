parameters = [0.1, 30.0, 1.0, 0.1, 0.5, 0.1, 0.015, 0.5, 3.0, 0.0, 0.0, 0.0, 0.0, 10.0, 0.0, 0.32, 0.032, 0.5, 0.005, 0.0, 3.0, 0.0, 1.0, 0.0];

[data1, bdata1, btype1, desc1] = FindCurve('CjaP', 'EQ', [0.0 2.12019 0.892170 11.3152 0.0], 0.5, [0.0 1.0], parameters, {'par1', '10', 'EXTfun', '1', 'EXTpar', '10'}, 'clean', 'force');
[data2, bdata2, btype2, desc2] = FindCurve('CjaP', 'EXT', bdata1(1:6), 0.1, [0.0 1.0 0 70], parameters, {'par1', '10', 'par2', '13', 'EXTfun', '1', 'EXTpar', '10'});
[data3, bdata3, btype3, desc3] = FindCurve('CjaP', 'EXT', bdata1(1:6), -0.1, [0.0 1.0 0 70], parameters, {'par1', '10', 'par2', '13', 'EXTfun', '1', 'EXTpar', '10'});

plot(data2(:,1),data2(:,6))
hold on
plot(data3(:,1),data3(:,6))
xlim([0, 0.4])
ylim([0, 70])
