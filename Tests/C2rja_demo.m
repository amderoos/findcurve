
clear 
close all
scrsz = get(0,'ScreenSize');
left= 0.1;
width=0.85;
    
disp(' ');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

init = [2.07278E-01     2.07266E-01     2.00000E+00     1.00656E-05     8.00686E-08];
parameters = [1.0, 2.0, 2.0, 0.5, 0.1, 10.0, 1.0, 0.0, 0.1, 0.0, 0.0, 0.0, 0.0, 10.0];
%parameters = [1.0, 2.0, 2.0, 0.5, 0.1, 10.0, 1.0, 0.0, 0.1, 0.0, 0.0, 0.0, 0.0, 10.0];

disp(' '); disp(' ');
disp(' ');
disp('Consumer-resource equilibrium as a function of R1max for default parameters');
disp(' ');
prompt = '>> [data1, bdata1, btype1, desc1] = FindCurve(''C2rja'', ''EQ'', init, 0.2, [0.0 3.0], parameters, {}, ''clean'')';
str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end
if strcmp(str, 'q') || strcmp(str, 'Q')
    error('Test script interrupted by user');
elseif ~strcmp(str, 's') && ~strcmp(str, 'S')
    [data1, bdata1, btype1, desc1] = FindCurve('C2rja', 'EQ', init, 0.2, [0.0 3.0], parameters, {}, 'clean');
    
    hfig = figure('Position',[1 scrsz(4) scrsz(3)/2 scrsz(4)/2]);
    h1 = axes('Position',[left 0.12 width width], 'LineWidth', 1, 'XLim', [0.0,3.0], 'YLim', [0.0, 1.2]);
    hx1=xlabel('Maximum density resource 1');
    hy1=ylabel('Average consumer density');
    set(gca, 'fontsize', 16);
    set(hx1, 'fontsize', 18);
    set(hy1, 'fontsize', 18);
    
    % Plot the data
    axes(h1);
    hold on
    plot(data1(:,1), data1(:,9),'Color',[.6 0 0]);
    plot(bdata1(:,1), bdata1(:,9),'*', 'Color','red');
    text(bdata1(:,1), bdata1(:,9),btype1);

    plot(data1(:,1), data1(:,10),'Color',[0 0 .6]);
    plot(bdata1(:,1), bdata1(:,10),'*', 'Color','red');
    text(bdata1(:,1), bdata1(:,10),btype1);
end

