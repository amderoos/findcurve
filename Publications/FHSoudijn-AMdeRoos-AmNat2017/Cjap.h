/***
   NAME
     Cjap
   PURPOSE
     Module computes the fixed point dynamics of a stage-strcutured biomass model
     with a single fluctuating resource and a predator

    Last modification: FHS - Aug 31, 2015
 ***/

#include "globals.h"
//=======	PROGRAM SETTINGS	================================================

#define RESFLUC			1										// Set resource dynamics to Semi-chemostat ( = 0) or fluctuating dynamics ( = 1)
//#define PI					3.1415926535897932	//define Pi

/*
 *===========================================================================
 * 		DEFINITION OF PROBLEM DIMENSIONS AND NUMERICAL SETTINGS
 *===========================================================================
 */
// Dimension settings: Required
#define EQUATIONS_DIM           4
#define EXTRAOUTPUT_DIM         8
#define	PARAMETER_NR           22

// Numerical settings: Optional (default values adopted otherwise)
#define ODESOLVE_MAX_STEP   	1.0         // Largest step size in odesolver

#define DYTOL                 1.0E-6		// Variable tolerance
#define RHSTOL                1.0E-8		// Function tolerance
#define ALLOWNEGATIVE         0           // Negative solution components allowed?

/*
 *===========================================================================
 * 		DEFINITION OF ALIASES
 *===========================================================================
 */
// Define aliases for the parameters
#define DELTAR		parameter[ 0]
#define RMAX			parameter[ 1]

//parameters for consumer general
#define MC			parameter[ 2]
#define TC			parameter[ 3]
#define SIGMAC	parameter[ 4]
#define ZC			parameter[ 5]	// Bodysize maturation ratio
#define MUC			parameter[ 6]	// Background mortality
#define QC			parameter[ 7]
#define HC			parameter[ 8]

#define MUCPLUS 		parameter[ 9]
#define MUCJPLUS		parameter[10]
#define	MUCAPLUS		parameter[11]

//parameters for predator
#define MP				parameter[12]
#define TP				parameter[13]
#define	SIGMAP		parameter[14]
#define MUP				parameter[15]
#define PHI				parameter[16]	// Preference for Juvenile stage of consumer
#define HP				parameter[17]

#define MUPLUSP			parameter[18]

#define	FUNCRESP		parameter[19]

#define ARESF				parameter[20]	//Controls amplitude of resource fluctuations
#define YRESF				parameter[21]	//Controls period of resource fluctuations
/*
 *===========================================================================
 * 		DEFINITION OF NAMES AND DEFAULT VALUES OF THE PARAMETERS
 *===========================================================================
 */
// At least two parameters should be specified in this array
char  *parameternames[PARAMETER_NR] =
    { "Delta", "Rmax", "Mc", "Tc", "Sigmac", "Zc", "Muc", "Qc", "Hc", "MucPlus", "MucjPlus", "MucaPlus",
    		"Mp", "Tp", "Sigmap", "Mup", "Phi", "Hp", "Muplusp", "FuncResp", "AresF","YresF"};

// These are the default parameters values
double	parameter[PARAMETER_NR] =
    { 0.1, 40.0, 1.0, 0.1, 0.5, 0.5, 0.015, 1.5, 3.0, 0.0, 0.0, 0.0,
      0.32, 0.032, 0.5, 0.005, 0.0, 3.0, 0.0, 1.0, 0.0, 10.0};

/*
 *===========================================================================
 * 		DEFINITION OF THE SYSTEM OF EQUATIONS TO SOLVE
 *===========================================================================
 *
 * Specify the threshold determining the end point of each discrete life
 * stage in individual life history as function of i-state and environment
 * variables for all individuals of all populations in every life stage
 *
 * Notice that the first index of the variable 'istate[][]' refers to the
 * number of the structured population, the second index refers to the
 * number of the individual state variable. The interpretation of the latter
 * is up to the user.
 *===========================================================================
 */

#undef	MAX_EXP
#define MAX_EXP		50.0

double	Maturation(double z, double nuj, double muj)

{
  double		logz, tmp, tres, matrate = 0.0;

  logz = log(z);
  tres  = muj/(1.0 - MAX_EXP/logz);
  if (nuj < tres) matrate = 0.0;
  else
    {
      tmp = 1.0 - muj/nuj;
      if (fabs(tmp) < 1.0E-6)
	matrate = tmp/2 - 1/logz;
      else
	matrate = tmp/(1.0 - exp(tmp*logz));
    }
  matrate  *= nuj;

  return matrate;
}

/*==========================================================================*/

// The ODE system defining the change in state variables during the growth period

#define ODEDIM		12
static double StoredVals[ODEDIM];
static int		OdeDim = ODEDIM;
//static int		Pi = PI

#define R			argument[0]		// Resource
#define CJ		argument[1]		// Juvenile consumers
#define CA		argument[2]		// Adult consumers
#define P			argument[3]		// Predator biomass

#define DRDT		derivative[0]	// Resource
#define DCJDT		derivative[1]	// Juvenile consumers
#define DCADT		derivative[2]	// Adult consumers
#define DPDT		derivative[3]	// Predator

void WithinSeason (double t, double *argument, double *derivative)
{
	static double               ingest_R = 0.0;
	static double               nu_J, nu_A;
	static double               mort_J, mort_A, maturation, encP;
#if (RESFLUC == 1)
	static double								rfluc;
#endif
	ingest_R = MC*((2 - QC)*CJ + QC*CA)*R/((1 - FUNCRESP) + FUNCRESP*(R + HC));

	nu_J     = SIGMAC*(2 - QC)*MC*R/((1 - FUNCRESP) + FUNCRESP*(R + HC)) - TC;
	nu_A     = SIGMAC*     QC *MC*R/((1 - FUNCRESP) + FUNCRESP*(R + HC)) - TC;

	encP = PHI*CJ + (1 - PHI)*CA;

	// Starvation mortality (max(nu_J, 0.0)-nu_J) included here !
	mort_J   = MUC + MUCJPLUS + MUCPLUS + max(-nu_J, 0.0) + MP*    PHI*P/((1 - FUNCRESP) + FUNCRESP*(encP + HP));
	mort_A   = MUC + MUCAPLUS + MUCPLUS + max(-nu_A, 0.0) + MP*(1 - PHI)*P/((1 - FUNCRESP) + FUNCRESP*(encP + HP));

	maturation = Maturation(ZC, nu_J, mort_J)*CJ;

#if (RESFLUC == 0)
	DRDT = DELTAR*(RMAX - R) - ingest_R;
#elif (RESFLUC == 1)
	rfluc = RMAX*(1 + ARESF *sin(2*PI*t / YRESF));
	DRDT = DELTAR*(rfluc - R) - ingest_R;
#endif

	DCJDT = max(nu_A, 0.0)*CA + max(nu_J, 0.0)*CJ - maturation - mort_J*CJ;
	DCADT =                     										maturation - mort_A*CA;
	DPDT  = (SIGMAP*MP*encP/((1 - FUNCRESP) + FUNCRESP*(encP + HP)) - TP - MUP - MUPLUSP)*P;

	// Integrate the following ODEs only for output purposes
	if (OdeDim == ODEDIM)
	{
		derivative[ 4] = R;
		derivative[ 5] = CJ;
		derivative[ 6] = CA;
		derivative[ 7] = CJ + CA;
		derivative[ 8] = P;
		derivative[ 9] = (SIGMAP*MP*encP/((1 - FUNCRESP) + FUNCRESP*(encP + HP)) - TP - MUP - MUPLUSP);
		derivative[10] = max(-nu_J, 0.0);
		derivative[11] = max(-nu_A, 0.0);
	}
	return;
}

/*===========================================================================*/

// Routine specifying the system of equalities from which to solve for
// R, J, A and P at equilibrium
#define PERIOD	1

int		Equations(double *argument, double *result)

{
	int			period;
	double		tval, tend, x[ODEDIM];

	//==============================================================================
	// Set the initial point for the ODEs

	memset(x, 0, ODEDIM*sizeof(double));
	x[0] = R;
	x[1] = CJ;
	x[2] = CA;
	x[3] = P;

	if (result) OdeDim = 4;
	else OdeDim = ODEDIM;
	tval = 0.0;
	tend = YRESF;

	for (period = 0; period < PERIOD; period++)
	{
		tend = (period+1)*YRESF;
		// Integrate up to end of the growing phase
		if (odesolve(x, OdeDim, &tval, tend, WithinSeason, NULL) == FAILURE)
		{
			ErrorMsg(__FILE__, __LINE__, "Integration failed!");
			return FAILURE;
		}
		if (!result) memcpy(StoredVals, x, ODEDIM*sizeof(double));
		//x[1] += x[4];				// Add reproductive mass to juveniles
		//x[4]  = 0.0;				// Reset reproductive mass
	}

	//==============================================================================
	// Compute the final values of the fixed point equation F(y)=0,

	if (result)
	{
		result[0]  = (R  - x[0]);
		result[1]  = (CJ - x[1]);
		result[2]  = (CA - x[2]);
		result[3]  = (P  - x[3]);
	}

	return SUCCES;
}

/*===========================================================================*/

// Define all variables to be written to the output file (column-organized ASCII file)

int		DefineExtraOutput(double *argument, double *ExtraOutput)

{
  // Invoke the routine that sets the right-hand side for setting the output variables
  if (Equations(argument, NULL) == FAILURE) return FAILURE;

  ExtraOutput[0] = StoredVals[ 4]/YRESF;	// Average resource
  ExtraOutput[1] = StoredVals[ 5]/YRESF;	// Average Cj
  ExtraOutput[2] = StoredVals[ 6]/YRESF;	// Avergae Ca
  ExtraOutput[3] = StoredVals[ 7]/YRESF;	// Average Cj+Ca
  ExtraOutput[4] = StoredVals[ 8]/YRESF;	// Average P
  ExtraOutput[5] = StoredVals[ 9]/YRESF;	// P.C. growth rate P
  ExtraOutput[6] = StoredVals[10]/YRESF;	// Average starvation mortality Cj
  ExtraOutput[7] = StoredVals[11]/YRESF;	// Average starvation mortality Ca

  return SUCCES;
}


/*
 *===========================================================================
 *  Default values for several indices used in the bifurcation:
 *
 *  Bifparone : Index of the first free parameter in the bifurcation
 *  Bifpartwo : Index of the second free parameter in the bifurcation
 *  EXTfun    : Index of the element in the ExtraOutput[] vector, specifying
 *  						the function value for which to monitor or continue a maximum
 *  						or minimum value along the curve
 *  EXTpar    : Index of the parameter, with respect to which to determine the
 *  						derivative of the function value contained in ExtraOutput[EXTfun]
 *  						along the curve
 *
 *  These indices can also be set via command-line options
 *===========================================================================
 */

void SetBifPars(void)
{
  Bifparone  = 18;				// Additional predator mortality
  Bifpartwo  = 20	;			  // Interval resource fluctuations
  //EXTfun     = 1;				  // Juvenile biomass overcompensation (average)
  //EXTfun     = 2;				// Adult biomass overcompensation (average)
  //EXTpar     = 9;				  // W.r.t. stage-independent mortality

  return;
}


/*==============================================================================*/
